const videoItems = [
    {
        videoId: 1,
        thumbnail: "/img/video/video-tmb-1.jpg",
        flag: "New!",
        videoСondition: "Live",
        liked: true,
        categoryTags: ["Joga", "Meditation", "Easy", "Workshop"],
        title: "Rīta joga visam ķermenim 5 min Vinyasa flow for full joga visa Yoga 1",
        description: "",
        userId: 1,
        category: "Yoga"
    },
    {
        videoId: 2,
        thumbnail: "/img/video/video-tmb-2.jpg",
        flag: "Popular",
        videoСondition: "Offline",
        liked: false,
        categoryTags: ["Joga", "Meditation", "Easy", "Workshop"],
        title: "Rīta joga visam ķermenim 5 min Vinyasa flow for full joga visa Yoga 2",
        description: "",
        userId: 1,
        category: "Yoga"
    },
    {
        videoId: 3,
        thumbnail: "/img/video/video-tmb-3.jpg",
        flag: "Popular",
        videoСondition: "Planned",
        liked: false,
        categoryTags: ["Joga", "Meditation", "Easy", "Workshop"],
        title: "Rīta joga visam ķermenim 5 min Vinyasa flow for full joga visa Yoga 3",
        description: "",
        userId: 1,
        category: "Yoga"
    },
    {
        videoId: 4,
        thumbnail: "/img/video/video-tmb-1.jpg",
        flag: "Popular",
        videoСondition: "Planned",
        liked: false,
        categoryTags: ["Joga", "Meditation", "Easy", "Workshop"],
        title: "Rīta joga visam ķermenim 5 min Vinyasa flow for full joga visa Yoga 4",
        description: "",
        userId: 1,
        category: "Yoga"
    },
    {
        videoId: 5,
        thumbnail: "/img/video/video-tmb-2.jpg",
        flag: "Popular",
        videoСondition: "Offline",
        liked: false,
        categoryTags: ["Joga", "Meditation", "Easy", "Workshop"],
        title: "Rīta joga visam ķermenim 5 min Vinyasa flow for full joga visa Yoga 5",
        description: "",
        userId: 1,
        category: "Yoga"
    },
    {
        videoId: 6,
        thumbnail: "/img/video/video-tmb-3.jpg",
        flag: "Popular",
        videoСondition: "Offline",
        liked: false,
        categoryTags: ["Joga", "Meditation", "Easy", "Workshop"],
        title: "Elisabeth Vinyasa flow for full joga visa Yoga 6",
        description: "",
        userId: 1,
        category: "Yoga"
    },
    {
        videoId: 7,
        thumbnail: "/img/video/video-tmb-2.jpg",
        flag: "Popular",
        videoСondition: "Offline",
        liked: false,
        categoryTags: ["Joga", "Meditation", "Easy", "Workshop"],
        title: "Elisabeth of Constantinople Vinyasa flow for full joga visa Yoga 7",
        description: "",
        userId: 1,
        category: "Yoga"
    },
    {
        videoId: 8,
        thumbnail: "/img/video/video-tmb-3.jpg",
        flag: "Popular",
        videoСondition: "Offline",
        liked: false,
        categoryTags: ["Joga", "Meditation", "Easy", "Workshop"],
        title: "Elisabeth professional Vinyasa flow for full joga visa Yoga 8",
        description: "",
        userId: 1,
        category: "Yoga"
    },
    {
        videoId: 9,
        thumbnail: "/img/video/video-tmb-big-1.jpg",
        flag: "New!",
        videoСondition: "Planned",
        liked: false,
        categoryTags: ["Joga", "Meditation", "Easy", "Workshop"],
        title: "Elisabeth tighten the body Vinyasa flow for full joga visa Yoga 9",
        description: "The best morning workout you can do to wake up your body is a combination of heart raising exercises, strength and mixing up stretching yoga poses throughout the workout.",
        userId: 2,
        category: "Yoga"
    },
    {
        videoId: 10,
        thumbnail: "/img/video/video-tmb-big-4.jpg",
        flag: "Popular",
        videoСondition: "Planned",
        liked: false,
        categoryTags: ["Joga", "Meditation", "Easy", "Workshop"],
        title: "Elisabeth tighten the body, model the muscle corset visa  Yoga 10",
        description: "The best morning workout you can do to wake up your body is a combination of heart raising exercises, strength and mixing up stretching yoga poses throughout the workout.",
        userId: 2,
        category: "Yoga"
    },



    {
        videoId: 11,
        thumbnail: "/img/video/video-tmb-2.jpg",
        flag: "Popular",
        videoСondition: "Offline",
        liked: false,
        categoryTags: ["Joga", "Meditation", "Easy", "Workshop"],
        title: "Rīta joga visam ķermenim 5 min Vinyasa flow for full joga visa Fitness 11",
        description: "",
        userId: 1,
        category: "Fitness"
    },
    {
        videoId: 12,
        thumbnail: "/img/video/video-tmb-1.jpg",
        flag: "New!",
        videoСondition: "Live",
        liked: true,
        categoryTags: ["Joga", "Meditation", "Easy", "Workshop"],
        title: "Rīta joga visam ķermenim 5 min Vinyasa flow for full joga visa Fitness 12",
        description: "",
        userId: 1,
        category: "Fitness"

    },
    {
        videoId: 13,
        thumbnail: "/img/video/video-tmb-3.jpg",
        flag: "Popular",
        videoСondition: "Planned",
        liked: false,
        categoryTags: ["Joga", "Meditation", "Easy", "Workshop"],
        title: "Rīta joga visam ķermenim 5 min Vinyasa flow for full joga visa Fitness 13",
        description: "",
        userId: 1,
        category: "Fitness"
    },
    {
        videoId: 14,
        thumbnail: "/img/video/video-tmb-2.jpg",
        flag: "Popular",
        videoСondition: "Offline",
        liked: false,
        categoryTags: ["Joga", "Meditation", "Easy", "Workshop"],
        title: "Rīta joga visam ķermenim 5 min Vinyasa flow for full joga visa Fitness 14",
        description: "",
        userId: 1,
        category: "Fitness"
    },
    {
        videoId: 15,
        thumbnail: "/img/video/video-tmb-3.jpg",
        flag: "Popular",
        videoСondition: "Offline",
        liked: false,
        categoryTags: ["Joga", "Meditation", "Easy", "Workshop"],
        title: "Rīta joga visam ķermenim 5 min Vinyasa flow for full joga visa Fitness 15",
        description: "",
        userId: 1,
        category: "Fitness"
    },
    {
        videoId: 16,
        thumbnail: "/img/video/video-tmb-1.jpg",
        flag: "Popular",
        videoСondition: "Planned",
        liked: false,
        categoryTags: ["Joga", "Meditation", "Easy", "Workshop"],
        title: "Rīta joga visam ķermenim 5 min Vinyasa flow for full joga visa Fitness 16",
        description: "",
        userId: 1,
        category: "Fitness"
    },
    {
        videoId: 17,
        thumbnail: "/img/video/video-tmb-2.jpg",
        flag: "Popular",
        videoСondition: "Offline",
        liked: false,
        categoryTags: ["Joga", "Meditation", "Easy", "Workshop"],
        title: "Rīta joga visam ķermenim 5 min Vinyasa flow for full joga visa Fitness 17",
        description: "",
        userId: 1,
        category: "Fitness"
    },
    {
        videoId: 18,
        thumbnail: "/img/video/video-tmb-3.jpg",
        flag: "Popular",
        videoСondition: "Offline",
        liked: false,
        categoryTags: ["Joga", "Meditation", "Easy", "Workshop"],
        title: "Rīta joga visam ķermenim 5 min Vinyasa flow for full joga visa Fitness 18",
        description: "",
        userId: 1,
        category: "Fitness"
    },




    {
        videoId: 19,
        thumbnail: "/img/video/video-tmb-3.jpg",
        flag: "Popular",
        videoСondition: "Planned",
        liked: false,
        categoryTags: ["Joga", "Meditation", "Easy", "Workshop"],
        title: "Rīta joga visam ķermenim 5 min Vinyasa flow for full joga visa Meditation 19",
        description: "",
        userId: 1,
        category: "Meditation"
    },
    {
        videoId: 20,
        thumbnail: "/img/video/video-tmb-1.jpg",
        flag: "New!",
        videoСondition: "Live",
        liked: true,
        categoryTags: ["Joga", "Meditation", "Easy", "Workshop"],
        title: "Rīta joga visam ķermenim 5 min Vinyasa flow for full joga visa Meditation 20",
        description: "",
        userId: 1,
        category: "Meditation"

    },
    {
        videoId: 21,
        thumbnail: "/img/video/video-tmb-2.jpg",
        flag: "Popular",
        videoСondition: "Live",
        liked: false,
        categoryTags: ["Joga", "Meditation", "Easy", "Workshop"],
        title: "Rīta joga visam ķermenim 5 min Vinyasa flow for full joga visa Meditation 21",
        description: "",
        userId: 1,
        category: "Meditation"
    },
    {
        videoId: 22,
        thumbnail: "/img/video/video-tmb-3.jpg",
        flag: "Popular",
        videoСondition: "Live",
        liked: false,
        categoryTags: ["Joga", "Meditation", "Easy", "Workshop"],
        title: "Rīta joga visam ķermenim 5 min Vinyasa flow for full joga visa Meditation 22",
        description: "",
        userId: 1,
        category: "Meditation"
    },
    {
        videoId: 23,
        thumbnail: "/img/video/video-tmb-2.jpg",
        flag: "Popular",
        videoСondition: "Live",
        liked: false,
        categoryTags: ["Joga", "Meditation", "Easy", "Workshop"],
        title: "Rīta joga visam ķermenim 5 min Vinyasa flow for full joga visa Meditation 23",
        description: "",
        userId: 1,
        category: "Meditation"
    },
    {
        videoId: 24,
        thumbnail: "/img/video/video-tmb-1.jpg",
        flag: "Popular",
        videoСondition: "Live",
        liked: false,
        categoryTags: ["Joga", "Meditation", "Easy", "Workshop"],
        title: "Rīta joga visam ķermenim 5 min Vinyasa flow for full joga visa Meditation 24",
        description: "",
        userId: 1,
        category: "Meditation"
    },
    {
        videoId: 25,
        thumbnail: "/img/video/video-tmb-2.jpg",
        flag: "Popular",
        videoСondition: "Live",
        liked: false,
        categoryTags: ["Joga", "Meditation", "Easy", "Workshop"],
        title: "Rīta joga visam ķermenim 5 min Vinyasa flow for full joga visa Meditation 25",
        description: "",
        userId: 1,
        category: "Meditation"
    },
    {
        videoId: 26,
        thumbnail: "/img/video/video-tmb-3.jpg",
        flag: "Popular",
        videoСondition: "Live",
        liked: false,
        categoryTags: ["Joga", "Meditation", "Easy", "Workshop"],
        title: "Rīta joga visam ķermenim 5 min Vinyasa flow for full joga visa M_ditation 26",
        description: "",
        userId: 1,
        category: "Meditation"
    },




    {
        videoId: 27,
        thumbnail: "/img/video/video-tmb-1.jpg",
        flag: "New!",
        videoСondition: "Live",
        liked: true,
        categoryTags: ["Joga", "Meditation", "Easy", "Workshop"],
        title: "Rīta joga visam ķermenim 5 min Vinyasa flow for full joga visa Dance 27",
        description: "",
        userId: 1,
        category: "Dance"

    },
    {
        videoId: 28,
        thumbnail: "/img/video/video-tmb-2.jpg",
        flag: "Popular",
        videoСondition: "Live",
        liked: false,
        categoryTags: ["Joga", "Meditation", "Easy", "Workshop"],
        title: "Rīta joga visam ķermenim 5 min Vinyasa flow for full joga visa Dance 28",
        description: "",
        userId: 1,
        category: "Dance"
    },
    {
        videoId: 29,
        thumbnail: "/img/video/video-tmb-3.jpg",
        flag: "Popular",
        videoСondition: "Live",
        liked: false,
        categoryTags: ["Joga", "Meditation", "Easy", "Workshop"],
        title: "Rīta joga visam ķermenim 5 min Vinyasa flow for full joga visa Dance 29",
        description: "",
        userId: 1,
        category: "Dance"
    },
    {
        videoId: 30,
        thumbnail: "/img/video/video-tmb-1.jpg",
        flag: "Popular",
        videoСondition: "Planned",
        liked: false,
        categoryTags: ["Joga", "Meditation", "Easy", "Workshop"],
        title: "Rīta joga visam ķermenim 5 min Vinyasa flow for full joga visa Dance 30",
        description: "",
        userId: 1,
        category: "Dance"
    },
    {
        videoId: 31,
        thumbnail: "/img/video/video-tmb-3.jpg",
        flag: "Popular",
        videoСondition: "Offline",
        liked: false,
        categoryTags: ["Joga", "Meditation", "Easy", "Workshop"],
        title: "Rīta joga visam ķermenim 5 min Vinyasa flow for full joga visa Dance 31",
        description: "",
        userId: 1,
        category: "Dance"
    },
    {
        videoId: 32,
        thumbnail: "/img/video/video-tmb-2.jpg",
        flag: "Popular",
        videoСondition: "Offline",
        liked: false,
        categoryTags: ["Joga", "Meditation", "Easy", "Workshop"],
        title: "Rīta joga visam ķermenim 5 min Vinyasa flow for full joga visa Dance 32",
        description: "",
        userId: 1,
        category: "Dance"
    },
    {
        videoId: 33,
        thumbnail: "/img/video/video-tmb-2.jpg",
        flag: "Popular",
        videoСondition: "Offline",
        liked: false,
        categoryTags: ["Joga", "Meditation", "Easy", "Workshop"],
        title: "Elizabeth joga visam ķermenim 5 min Vinyasa flow for full joga visa Dance 33",
        description: "",
        userId: 1,
        category: "Dance"
    },
    {
        videoId: 34,
        thumbnail: "/img/video/video-tmb-3.jpg",
        flag: "Popular",
        videoСondition: "Offline",
        liked: false,
        categoryTags: ["Joga", "Meditation", "Easy", "Workshop"],
        title: "Elizabeth joga visam ķermenim 5 min Vinyasa flow for full joga visa Dance 34",
        description: "",
        userId: 1,
        category: "Dance"
    },
    {
        videoId: 35,
        thumbnail: "/img/video/video-tmb-2.jpg",
        flag: "Popular",
        videoСondition: "Offline",
        liked: false,
        categoryTags: ["Joga", "Meditation", "Easy", "Workshop"],
        title: "Elisabeth work out muscle stabilizers that are rarely used in life Yoga 35",
        description: "",
        userId: 1,
        category: "Yoga"
    },
]

function createVideoItems(countCreation, categoryName, flagName) {
    const countCondition = videoItems.length + countCreation

    for(let videoId = videoItems.length + 1; videoId < countCondition; videoId++) {
        videoItems.push({
            videoId,
            thumbnail: "/img/video/video-tmb-2.jpg",
            flag: flagName,
            videoСondition: "Offline",
            liked: false,
            categoryTags: ["Joga", "Meditation", "Easy", "Workshop"],
            title: `Rīta joga visam ķermenim 5 min Vinyasa flow for full joga visa ${categoryName} ${videoId}`,
            description: "",
            userId: 1,
            category: categoryName
        })
    }
}
createVideoItems(20, "Yoga", "Popular")
createVideoItems(20, "Yoga", "New!")


import Dayjs from 'dayjs'
// console.log('this.$api.dayjs', Dayjs('1999-01-01').fromNow(true) )




const watchingNowApi = [
    {
        videoId: 1,
        watchingNow: 9876543,
    },
    {
        videoId: 12,
        watchingNow: 1240,
    },
    {
        videoId: 20,
        watchingNow: 44187,
    },
    {
        videoId: 21,
        watchingNow: 226,
    },
    {
        videoId: 22,
        watchingNow: 6386,
    },
    {
        videoId: 23,
        watchingNow: 4490,
    },
    {
        videoId: 24,
        watchingNow: 7338,
    },
    {
        videoId: 25,
        watchingNow: 43,
    },
    {
        videoId: 26,
        watchingNow: 1298,
    },
    {
        videoId: 27,
        watchingNow: 2378,
    },
    {
        videoId: 28,
        watchingNow: 878,
    },
    {
        videoId: 29,
        watchingNow: 1128,
    },
    {
        videoId: 30,
        watchingNow: 2332,
    },
    {
        videoId: 27,
        watchingNow: 37495,
    }
]
const scheduledVideos = [
    {
        videoId: 1,
        dateStart: "Today at 8 pm",
    },
    {
        videoId: 2,
        dateStart: "Today at 4 pm",
    },
    {
        videoId: 3,
        dateStart: "Today at 4 pm",
    },
    {
        videoId: 4,
        dateStart: "Today at 4 pm",
    },
    {
        videoId: 5,
        dateStart: "Today at 4 pm",
    },
    {
        videoId: 6,
        dateStart: "Today at 4 pm",
    },
    {
        videoId: 7,
        dateStart: "Today at 4 pm",
    },
    {
        videoId: 8,
        dateStart: "Today at 4 pm",
    },
    {
        videoId: 9,
        dateStart: "Today at 4 pm",
    },
    {
        videoId: 10,
        dateStart: "Today at 4 pm",
    },
    {
        videoId: 11,
        dateStart: "Today at 8 pm",
    },
    {
        videoId: 12,
        dateStart: "Today at 4 pm",
    },
    {
        videoId: 13,
        dateStart: "Today at 4 pm",
    },
    {
        videoId: 14,
        dateStart: "Today at 4 pm",
    },
    {
        videoId: 15,
        dateStart: "Today at 4 pm",
    },
    {
        videoId: 16,
        dateStart: "Today at 4 pm",
    },
    {
        videoId: 17,
        dateStart: "Today at 4 pm",
    },
    {
        videoId: 18,
        dateStart: "Today at 4 pm",
    },
    {
        videoId: 19,
        dateStart: "Today at 4 pm",
    },
    {
        videoId: 20,
        dateStart: "Today at 4 pm",
    },
    {
        videoId: 21,
        dateStart: "Today at 8 pm",
    },
    {
        videoId: 22,
        dateStart: "Today at 4 pm",
    },
    {
        videoId: 23,
        dateStart: "Today at 4 pm",
    },
    {
        videoId: 24,
        dateStart: "Today at 4 pm",
    },
    {
        videoId: 25,
        dateStart: "Today at 4 pm",
    },
    {
        videoId: 26,
        dateStart: "Today at 4 pm",
    },
    {
        videoId: 27,
        dateStart: "Today at 4 pm",
    },
    {
        videoId: 28,
        dateStart: "Today at 4 pm",
    },
    {
        videoId: 29,
        dateStart: "Today at 4 pm",
    },
    {
        videoId: 30,
        dateStart: "Today at 4 pm",
    },
    {
        videoId: 101,
        dateStart: "Today at 4 pm",
    }
]
const users = [
    {
        userId: 1,
        avatarImgUrl: "/img/users/avatar-1.png",
        firstName: "Elizabeth",
        lastName: "",
        city: "Constantinople"
    },
    {
        userId: 2,
        avatarImgUrl: "/img/users/avatar-2.png",
        firstName: "Maria",
        lastName: "Frolova",
        city: ""
    },
    {
        userId: 3,
        avatarImgUrl: "/img/users/avatar-2.png",
        firstName: "Anastasiya",
        lastName: "Madjahuddinova",
        city: ""
    },
    {
        userId: 4,
        avatarImgUrl: "/img/users/avatar-5.jpg",
        fullName: "Daria Frolova Gronox from Constantinople",
        followers: 741566,
        online: true
    },
    {
        userId: 5,
        avatarImgUrl: "/img/users/avatar-7.jpg",
        fullName: "Polly Freedom from Constantinople",
        followers: 314774,
        online: true
    },
    {
        userId: 6,
        avatarImgUrl: "/img/users/avatar-8.jpg",
        fullName: "Daria Frolova Gronox from Constantinople",
        followers: 155668,
        online: true
    },
    {
        userId: 7,
        avatarImgUrl: "/img/users/avatar-9.jpg",
        fullName: "Polly Freedom from Constantinople",
        followers: 544554,
        online: true
    },
    {
        userId: 8,
        avatarImgUrl: "/img/users/avatar-8.jpg",
        fullName: "Steven Carpenter",
        followers: 236843,
        online: true
    },
    {
        userId: 9,
        avatarImgUrl: "/img/users/avatar-7.jpg",
        fullName: "Kate Middlton",
        followers: 246789,
        online: true
    },
    {
        userId: 10,
        avatarImgUrl: "/img/users/avatar-5.jpg",
        fullName: "Steven Carpenter",
        followers: 567,
        online: true
    },
    {
        userId: 11,
        avatarImgUrl: "/img/users/avatar-9.jpg",
        fullName: "Kate Middlton",
        followers: 7654321,
        online: true
    },
]
// const videoNames = videoItems.map(video => video.title)
function createPlannedVideoItems(countCreation, categoryName, flagName, startTime, step) {
    const countCondition = videoItems.length + countCreation

    let currentTime = startTime
    for(let videoId = videoItems.length + 1; videoId < countCondition; videoId++) {
        videoItems.push({
            videoId,
            thumbnail: "/img/video/video-tmb-2.jpg",
            flag: flagName,
            videoСondition: "Planned",
            liked: false,
            categoryTags: ["Joga", "Meditation", "Easy", "Workshop"],
            title: `Rīta joga visam ķermenim 5 min Vinyasa flo wVinyasa flow for full joga for full joga visa ${categoryName} ${videoId}`,
            description: "",
            userId: 1,
            category: categoryName,
            date: Dayjs().add(currentTime, 'm')
        })

        currentTime += step
    }
}
function createOnlineVideoItems(countCreation, categoryName, flagName, startTime, step) {
    const countCondition = videoItems.length + countCreation

    let currentTime = startTime
    for(let videoId = videoItems.length + 1; videoId < countCondition; videoId++) {
        videoItems.push({
            videoId,
            thumbnail: "/img/video/video-tmb-2.jpg",
            flag: flagName,
            videoСondition: "Live",
            liked: false,
            categoryTags: ["Joga", "Meditation", "Easy", "Workshop"],
            title: `Rīta joga visam ķermenim 5 min Vinyasa flow for full joga Vinyasa flow for full joga visa ${categoryName} ${videoId}`,
            description: "",
            userId: 1,
            category: categoryName,
            date: Dayjs().add(currentTime, 'm')
        })
        watchingNowApi.push({
            videoId,
            watchingNow: 2332,
        })
        currentTime += step
    }
}
createPlannedVideoItems(10, "Stretching", "Popular", -500, 10)
// createPlannedVideoItems(10, "Stretching", "Popular", -900, 10)
createPlannedVideoItems(20, "Stretching", "Popular", -1440, 10)
createPlannedVideoItems(20, "Stretching", "Popular", -1440 * 2, 10)
createPlannedVideoItems(20, "Stretching", "Popular", -1440 * 3, 10)
// createPlannedVideoItems(40, "Stretching", "Popular", -270, 10)
createPlannedVideoItems(2, "Stretching", "Popular", -5, 5)
createOnlineVideoItems(4, "Easy", "Popular", 0, 1)
createPlannedVideoItems(40, "Stretching", "Popular", 0, 10)

createPlannedVideoItems(10, "Stretching", "Popular", 720, 10)
createPlannedVideoItems(10, "Stretching", "Popular", 1440, 10)
createPlannedVideoItems(10, "Stretching", "Popular", 1440 * 2, 10)
createPlannedVideoItems(10, "Stretching", "Popular", 1440 * 3, 10)




// console.log('scheduledVideos', scheduledVideos)

export { users, videoItems, watchingNowApi, scheduledVideos }