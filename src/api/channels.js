import { postRequest, getRequest, putRequest, deleteRequest } from './requests'

const routePath = 'channel/channels'

export default {
    async create(data) {
        return await postRequest(routePath, data)
    },
    async getAll() {
        return await getRequest(routePath, { params: { take: 500 } })
    },
    async getMy() {
        return await getRequest(`${routePath}/my`)
    },
    async getAllPrivate() {
        return await getRequest(`${routePath}/private`, { params: { take: 500 } })
    },
    async get(id) {
        return await getRequest(`${routePath}/${id}`)
    },
    async update(id, data) {
        return await putRequest(`${routePath}/${id}`, data)
    },
    async delete(id) {
        return await deleteRequest(`${routePath}/${id}`)
    },
    async restore(id) {
        return await putRequest(`${routePath}/${id}/restore`)
    },
    async follow(id) {
        return await putRequest(`${routePath}/${id}/follow`)
    },
    async unfollow(id) {
        return await deleteRequest(`${routePath}/${id}/follow`)
    },
    async subscribe(id, monthCount) {
        return await putRequest(`${routePath}/${id}/subscribe`, monthCount)
    },
}