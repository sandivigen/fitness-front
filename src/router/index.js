import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store'
import Home from '@/views/Home'
import Popular from '@/views/Popular'
import Recommendation from '@/views/Recommendation'
import Timetable from '@/views/Timetable'
import History from '@/views/History'
import Subscriptions from '@/views/Subscriptions'
import Categories from "@/views/Categories"
import Category from "@/views/Category"
import CategorySearchResult from "@/views/CategorySearchResult"
import HeaderSearchResult from "@/views/HeaderSearchResult"
import Profile from "@/views/Profile"

import AddChannel from "@/views/channels/AddChannel"
import AllChannels from "@/views/channels/AllChannels"
import MyChannels from "@/views/channels/MyChannels"
import ChannelApi from "@/views/channels/Channel"

import Stream from "@/components/home/translation.vue"
import Stream2 from "@/components/home/testStream.vue"
import AddStream from "@/views/streams/AddStream"
import AddBroadcast from "@/views/streams/AddBroadcast"
import Broadcasts from "@/views/streams/Broadcasts"
import Broadcast from "@/views/streams/Broadcast"
import ShowBroadcast from "@/views/streams/ShowBroadcast"
import Channel from "@/views/Channel"


Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'home',
        component: Home,
        meta: {
            title: 'Home'
        }
    },
    {
        path: '/popular',
        name: 'popular',
        component: Popular,
        meta: {
            title: 'Popular'
        }
    },
    {
        path: '/recommendation',
        name: 'recommendation',
        component: Recommendation,
        meta: {
            title: 'Recommendation'
        }
    },
    {
        path: '/timetable',
        name: 'timetable',
        component: Timetable,
        meta: {
            title: 'Timetable'
        }
    },
    {
        path: '/history',
        name: 'history',
        component: History,
        meta: {
            title: 'History'
        }
    },
    {
        path: '/subscriptions',
        name: 'subscriptions',
        component: Subscriptions,
        meta: {
            title: 'Subscriptions'
        }
    },
    {
        path: '/categories',
        name: 'categories',
        component: Categories,
        meta: {
            title: 'Categories'
        }
    },
    {
        path: "/category/:categoryName",
        name: 'category',
        component: Category,
        meta: {
            awaitTitle: true
        }
    },
    {
        path: "/category/:categoryName/category-search-result",
        name: 'CategorySearchResult',
        component: CategorySearchResult,
        meta: {
            title: 'Category Search Result'
        }
    },
    {
        path: "/search",
        name: 'HeaderSearchResult',
        component: HeaderSearchResult,
        meta: {
            title: 'Search Result'
        }
    },
    {
        path: "/profile",
        name: 'Profile',
        component: Profile,
        meta: {
            title: 'Profile'
        }
    },
    {
        path: "/add-channel",
        name: 'AddChannel',
        component: AddChannel,
        meta: {
            title: 'Add Channel'
        }
    },
    {
        path: "/channels",
        name: 'AllChannels',
        component: AllChannels,
        meta: {
            title: 'Channels'
        }
    },
    {
        path: "/my-channels",
        name: 'MyChannels',
        component: MyChannels,
        meta: {
            title: 'My Channels'
        }
    },
    {
        path: "/channel-api/:id",
        name: 'ChannelApi',
        component: ChannelApi,
        meta: {
            title: 'ChannelApi'
        }
    },
    {
        path: "/stream",
        name: 'Stream',
        component: Stream,
        meta: {
            title: 'Stream'
        }
    },
    {
        path: "/stream2",
        name: 'Stream2',
        component: Stream2,
        meta: {
            title: 'Stream2'
        }
    },
    {
        path: "/add-stream",
        name: 'AddStream',
        component: AddStream,
        meta: {
            title: 'Add Stream'
        }
    },
    {
        path: "/add-broadcast/:id",
        name: 'AddBroadcast',
        component: AddBroadcast,
        meta: {
            title: 'Add Broadcast'
        }
    },
    {
        path: "/broadcasts",
        name: 'Broadcasts',
        component: Broadcasts,
        meta: {
            title: 'Broadcasts'
        }
    },
    {
        path: "/broadcast/:id",
        name: 'Broadcast',
        component: Broadcast,
        meta: {
            title: 'Broadcast' // set title
        }
    },
    {
        path: "/show-broadcast/:id",
        name: 'ShowBroadcast',
        component: ShowBroadcast,
        meta: {
            title: 'ShowBroadcast' // set title
        }
    },
    {
        path: "/channel/:name",
        name: 'Channel',
        component: Channel,
        meta: {
            awaitTitle: true
        }
    },
    {
        path: "/channel/:name/:broadcastId",
        name: 'Channel',
        component: Channel,
        meta: {
            awaitTitle: true
        }
    },
]

const router = new VueRouter({
    mode: 'history',
    scrollBehavior() {
        return { x: 0, y: 0 }
    },
    // base: process.env.BASE_URL,
    routes
})

router.beforeEach((to, from, next) => {
    const hideSidebar = to.matched.some(record => record.path.startsWith('/profile'))
    store.commit('setVisibleSidebar', !hideSidebar)

    if (!to.meta?.awaitTitle) {
        const defaultTitle = store.state.defaultTitle
        document.title = to.meta?.title ? `${to.meta?.title} - ${defaultTitle}` : defaultTitle
    }
    next()
})

export default router