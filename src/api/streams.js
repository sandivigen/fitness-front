import { postRequest, putRequest } from './requests'

const routePath = 'stream/streams'

export default {
    async create(data) {
        return await postRequest(routePath, data)
    },
    async start(id) {
        return await putRequest(`${routePath}/${id}/start`)
    },
    async stop(id) {
        return await putRequest(`${routePath}/${id}/stop`)
    },
}