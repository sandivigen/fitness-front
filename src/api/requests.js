import axios from 'axios'
import store from '@/store'

const api = axios.create({
    baseURL: process.env.VUE_APP_API
})

api.interceptors.request.use(
    config => {
        const token = localStorage.getItem('token')
        if (token) {
            config.headers['Authorization'] = `Bearer ${token}`
        }
        return config
    },
    error => {
        return Promise.reject(error)
    }
)


// async function test() {
//     const res = await getRequest('http://vp.gcdn.co/browsercast?token=c803b78ff3a214993239213fba5aee59&streamId=20348_69975')

//     if (res instanceof Error) {
//         console.log('Error', res)
//         return res
//     }
//     console.log('Success', res)

// }

async function signup(user) {
    return await postRequest('auth/sessions/signupPassword', user)
}



async function getVerifyCode() {
    return await postRequest('auth/sessions/phoneVerifyCode', {phone: '+79055497542'})
}

async function login(user) {
    const res = await postRequest('auth/sessions/loginPassword', user)
    if (res instanceof Error) {
        return res
    }
    localStorage.setItem('token', res.token.token)
    const data = {
        fullName: res.user.fullName,
        username: res.user.username,
        email: res.user.email,
        gender: res.user.gender,
        avatarPath: res.user.avatarPath,
    }
    store.commit('setUser', data)
    store.dispatch('setSubscriptions')

    return res
}

async function signupFacebook(accessToken) {
    return await getRequest(`auth/sessions/signupFacebook?access_token=${accessToken}`)
}
async function loginFacebook(data) {
    localStorage.setItem('token', data.token.token)
    const userData = {
        fullName: data.user.fullName,
        username: data.user.username,
        email: data.user.email,
        gender: data.user.gender,
        avatarPath: data.user.avatarPath,
    }
    store.commit('setUser', userData)

    return userData
}

async function signupGoogle(accessToken) {
    return await getRequest(`auth/sessions/signupGoogle?access_token=${accessToken}`)
}
async function loginGoogle(data) {
    localStorage.setItem('token', data.token.token)
    const userData = {
        fullName: data.user.fullName,
        username: data.user.username,
        email: data.user.email,
        gender: data.user.gender,
        avatarPath: data.user.avatarPath,
    }
    store.commit('setUser', userData)

    return userData
}

function logout() {
    localStorage.removeItem('token')
    store.commit('logOut')
    return true
}

export default {
    signup,
    signupFacebook,
    signupGoogle,
    getVerifyCode,
    login,
    loginFacebook,
    loginGoogle,
    logout,
}

export async function getRequest(path, params = {}) {
    try {
        return (await api.get(path, params)).data
    } catch (e) {
        return e
    }
}

export async function postRequest(path, data, params = {}) {
    try {
        return (await api.post(path, data, params)).data
    } catch (e) {
        return e
    }
}

export async function putRequest(path, data, params = {}) {
    try {
        return (await api.put(path, data, params)).data
    } catch (e) {
        return e
    }
}

export async function deleteRequest(path, params = {}) {
    try {
        return (await api.delete(path, params)).data
    } catch (e) {
        return e
    }
}
