import { postRequest, getRequest, putRequest, deleteRequest } from './requests'

const routePath = 'stream/broadcasts'

export default {
    async create(data) {
        return await postRequest(routePath, data)
    },
    async all(params) {
        return await getRequest(routePath, { params })
    },
    async allPrivate(params) {
        params
        return await getRequest(`${routePath}/private`, { params })
        // return await getRequest(`${routePath}/private`, { params: { take: 100, order: 'ASC', page: 1 } })
        // return await getRequest('https://dev.fitliga.com/api/v0/stream/broadcasts/private?order=ASC&page=1&take=100')
        // return await getRequest('https://dev.fitliga.com/api/v0/stream/broadcasts/private?order=ASC&page=1&take=100 ')
        // return await getRequest('https://dev.fitliga.com/api/v0/stream/broadcasts/private?order=ASC&page=1&take=100')
    },
    async get(id) {
        return await getRequest(`${routePath}/${id}`)
    },
    async update(id, data) {
        return await putRequest(`${routePath}/${id}`, data)
    },
    async follow(id) {
        return await putRequest(`${routePath}/${id}/follow`)
    },
    async unfollow(id, data) {
        return await deleteRequest(`${routePath}/${id}/follow`, data)
    },
    async getCategories() {
        return await getRequest(`stream/broadcastCategories`, { params: { take: 500 } })
    },
    async start(id) {
        return await putRequest(`${routePath}/${id}/start`)
    },
    async stop(id) {
        return await putRequest(`${routePath}/${id}/stop`)
    },
}