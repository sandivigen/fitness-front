import { putRequest, getRequest, postRequest } from './requests'
import store from '@/store'

export default {
    async get() {
        return await getRequest('user/users/profile')
    },
    async update(user) {
        const res = await putRequest('user/users/profile', user)
        if (res instanceof Error) {
            return res
        }
        store.commit('setUser', user)
        return res
    },
    async updatePassword(data) {
        return await putRequest('auth/sessions/password', data)
    },
    async updateEmail(data) {
        return await putRequest('user/users/profile/email', data)
    },
    async uploadFiles(data, monthCount) {
        return await postRequest('uploader/user/azure', data, monthCount)
    }
}