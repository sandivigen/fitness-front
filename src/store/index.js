import Vue from 'vue'
import Vuex from 'vuex'
import api from '@/api/routes'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        user: {
            fullName: localStorage.getItem('fullName'),
            username: localStorage.getItem('username'),
            gender: localStorage.getItem('gender'),
            email: localStorage.getItem('email'),
            avatarPath: localStorage.getItem('avatarPath'),
            birthDate: localStorage.getItem('birthDate'),
        },
        isAuthorized: false,
        defaultTitle: 'Fitliga',
        showSidebar: true,
        subscriptions: [],
    },
    actions: {
        async setSubscriptions({ commit }) {
            let res = await api.channels.getAllPrivate()
            if(res?.data) {
                commit('setSubscriptions', res.data.filter(channel => channel.isSubscribe))
            }
        }
    },
    // getters: {
    //     getSubscriptions(state) {
    //         let sortableSubscriptions = state.subscriptions.sort((a, b) => {
    //             return a.subscribersCount - b.subscribersCount
    //         })
    //         return sortableSubscriptions
    //     }
    // },
    mutations: {
        setVisibleSidebar(state, value) {
            state.showSidebar = value
        },
        setUser(state, user) {
            localStorage.setItem('fullName', user.fullName || state.user.fullName)
            localStorage.setItem('username', user.username || state.user.username)
            localStorage.setItem('gender', user.gender || state.user.gender)
            localStorage.setItem('email', user.email || state.user.email)
            localStorage.setItem('avatarPath', user.avatarPath || state.user.avatarPath)
            localStorage.setItem('birthDate', user.birthDate || state.user.birthDate)
            Object.assign(state.user, user)

            state.isAuthorized = true
        },
        logOut(state) {
            localStorage.removeItem('fullName')
            localStorage.removeItem('username')
            localStorage.removeItem('gender')
            localStorage.removeItem('email')
            localStorage.removeItem('avatarPath')
            localStorage.removeItem('birthDate')

            state.user.fullName = ""
            state.user.username = ""
            state.user.gender = ""
            state.user.email = ""
            state.user.avatarPath = ""
            state.user.birthDate = ""

            state.subscriptions = []
            state.isAuthorized = false
        },
        isAuthorized(state) {
            if(state.user.username) {
                state.isAuthorized = true
            }
        },
        updateAvatar(state, avatarPath) {
            state.user.avatarPath = avatarPath
        },
        setSubscriptions(state, subscriptions) {
            state.subscriptions = subscriptions
        },
        addSubscription(state, channel) {
            state.subscriptions.push(channel)
        }
    }
})