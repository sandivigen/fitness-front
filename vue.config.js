const isProd = process.env.NODE_ENV === 'production'

module.exports = {
    css: {
        sourceMap: !isProd
    },
    devServer: {
        proxy: 'http://localhost:8080'
    }
}