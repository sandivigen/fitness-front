const smallVideoBlock = {
    data() {
        return {
            videoCount: null,
            videoRowCounter: 1,
            windowWidth: window.innerWidth,
        }
    },

    watch: {
        windowWidth() {
            this.checkSize()
        }
    },

    mounted() {
        window.addEventListener('resize', this.onResize)
        this.windowWidth = window.innerWidth
        this.checkSize()
    },

    beforeDestroy() { 
        window.removeEventListener('resize', this.onResize) 
    },


    methods: {
        checkSize() {
            if(window.innerWidth <= 1280) {
                this.videoCount = this.videoRowCounter * 3
            } else if(window.innerWidth <= 1584) {
                this.videoCount = this.videoRowCounter * 4
            } else {
                this.videoCount = this.videoRowCounter * 4
            }
        },

        onResize() {
            this.windowWidth = window.innerWidth
        }
    }
}

export default  smallVideoBlock 