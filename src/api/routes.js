import request from './requests'
import profile from './profile'
import channels from './channels'
import streams from './streams'
import broadcasts from './broadcasts'

export default {
    signup: request.signup,
    signupFacebook: request.signupFacebook,
    signupGoogle: request.signupGoogle,
    login: request.login,
    logout: request.logout,
    getVerifyCode: request.getVerifyCode,
    loginFacebook: request.loginFacebook,
    loginGoogle: request.loginGoogle,
    profile,
    channels,
    streams,
    broadcasts
}
