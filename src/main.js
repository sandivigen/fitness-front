import Vue from 'vue'
import App from "@/App"
import router from "@/router"
import store from '@/store'
import api from '@/api/routes'
import vClickOutside from 'v-click-outside'
import Vuelidate from 'vuelidate'
import { VueMaskDirective } from 'v-mask'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import Dayjs from 'dayjs'
import relativeTime from 'dayjs/plugin/relativeTime'
import 'dashjs'
import 'videojs-contrib-dash'



Dayjs.extend(relativeTime)

Vue.prototype.$dayjs = Dayjs
Vue.prototype.$api = api

Vue.use(vClickOutside)
Vue.use(Vuelidate)
Vue.use(VueAwesomeSwiper)
Vue.directive('mask', VueMaskDirective)

Vue.config.productionTip = false

new Vue({
    router,
    store,
    render: h => h(App),
}).$mount('#app')
